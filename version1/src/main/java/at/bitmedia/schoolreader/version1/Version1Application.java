package at.bitmedia.schoolreader.version1;

import at.bitmedia.schoolreader.version1.entity.FileStorageProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jmx.JmxAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;


@SpringBootApplication(exclude = JmxAutoConfiguration.class)
@EnableConfigurationProperties({
		FileStorageProperties.class
})
public class Version1Application {

	public static void main(String[] args) {
		SpringApplication.run(Version1Application.class, args);
	}
}
