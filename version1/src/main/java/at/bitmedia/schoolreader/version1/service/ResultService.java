package at.bitmedia.schoolreader.version1.service;

import at.bitmedia.schoolreader.version1.entity.Result;

public interface ResultService extends  TypicalService<Result>{
 Result insertResult(Result r);
}
